# Dynamic filtering
> Spring Boot version: 2.3.4.RELEASE  

Example project in Spring Boot for demonstrating dynamic filtering on DTOs.

## Background
There are cases when you want to ommit some properties from the response payload because you only 
need some of them. You can achieve this 'filtering' in two different ways: static or dynamic.

The static filtering can be done using the `@JsonIgnore` annotation on the property. The main use of this
approach is designed for cases when you want to ommit every single time that particular property. In case
you want to get back that property in some scenarios then it would simply not work. Here's the part when
**dynamic filtering** saves the situation. `Jackson` allows you to dynamically control which properties will
be filtered out/ignored when serializing your response.

## Test case
For demonstrating the dynamic filtering we have simple `GET` endpoint for fetching details of a vehicle based on
its id. The properties of the vehicle are the following:
- id: the id
- color: the color
- year: the year of fabrication
- make: the make (e.g. BMW, Mercedes, Audi etc.)

What we want to achieve is to control what properties we get back in the response by filtering out the unwanted
ones.

## Demo 
### 1. Full body response
Request:  
`GET http://localhost:8080/vehicles/1`  

Response:
```json
{
    "id": "1",
    "color": "black",
    "year": 2020,
    "make": "BMW"
}
```

### 2. Filtered body response
Request:  
`GET http://localhost:8080/vehicles/1?include=color`  

Response:
```json
{
    "color": "black"
}
```

## Snippet
The snippet created for this project can be found [here](https://gitlab.com/-/snippets/2032710).

## Contact
You can contact me at **georgeberar.contact@gmail.com**





