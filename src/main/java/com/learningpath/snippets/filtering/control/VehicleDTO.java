package com.learningpath.snippets.filtering.control;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * DTO for holding the details of a vehicle.
 *
 * @author George
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonFilter("VehicleDTOFilter")
public class VehicleDTO implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("color")
    private String color;

    @JsonProperty("year")
    private int year;

    @JsonProperty("make")
    private String make;

}

