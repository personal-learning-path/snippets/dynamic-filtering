package com.learningpath.snippets.filtering.control;


import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Control class for providing business behavior on vehicle resource.
 *
 * @author George
 */
@Service
public class VehicleControl {

    private static final List<VehicleDTO> vehicles;

    static {
        final VehicleDTO bmwVehicle = new VehicleDTO("1", "black", 2020, "BMW");
        final VehicleDTO miniVehicle = new VehicleDTO("2", "red", 2004, "Mini");
        final VehicleDTO mercedesVehicle = new VehicleDTO("3", "grey", 2019, "Mercedes");

        vehicles = Arrays.asList(bmwVehicle, miniVehicle, mercedesVehicle);
    }

    /**
     * Finds a vehicle by id.
     *
     * @param id - the id of the vehicle
     * @return an {@code Optional} with the vehicle DTO inside if found or empty otherwise
     */
    public Optional<VehicleDTO> findById(final String id) {
        return vehicles.stream().filter(vehicle -> vehicle.getId().equals(id)).findFirst();
    }

}
