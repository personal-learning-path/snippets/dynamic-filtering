package com.learningpath.snippets.filtering.boundary;


import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.learningpath.snippets.filtering.control.VehicleControl;
import com.learningpath.snippets.filtering.control.VehicleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static java.lang.String.format;

/**
 * Boundary for exposing endpoints of Vehicles API.
 *
 * @author George
 */
@RestController
@RequestMapping(path = "/vehicles")
public class VehiclesBoundary {


    @Autowired
    private VehicleControl vehicleControl;

    /**
     * Get a vehicle by id.
     * <p>
     * It provides the possibility to return only one property of the vehicle by using the optional query param {@code include}.
     *
     * @param id      - the id of the vehicle
     * @param include - optional query parameter for specifying one property of the vehicle which should be the only one
     *                present in the response
     * @return the vehicle if found
     */
    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<MappingJacksonValue> getVehicleById(
            @PathVariable(name = "id") final String id,
            @RequestParam(name = "include", required = false) final String include) {

        final MappingJacksonValue response;

        // find vehicle by id
        final Optional<VehicleDTO> existingVehicle = this.vehicleControl.findById(id);

        if (existingVehicle.isPresent()) {
            final VehicleDTO vehicle = existingVehicle.get();

            // check if the optional query param is given
            if (Optional.ofNullable(include).isPresent()) {
                // check if the specified value is valid
                try {
                    VehicleDTO.class.getDeclaredField(include);
                } catch (final NoSuchFieldException ex) {
                    throw new RuntimeException(format("Invalid value {%s} specified for 'include' param", include));
                }

                // define the filter to include only the requested property
                final SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept(include);

                // create a filter provider and add the created filter
                final FilterProvider filterProvider = new SimpleFilterProvider().addFilter("VehicleDTOFilter", filter);

                // define the mapping for the response DTO
                response = new MappingJacksonValue(vehicle);

                // specify the use of the filter
                response.setFilters(filterProvider);
            } else {
                // return the vehicle with all properties
                response = new MappingJacksonValue(vehicle);
                response.setFilters(new SimpleFilterProvider().addFilter("VehicleDTOFilter", SimpleBeanPropertyFilter.serializeAll()));
            }
        } else {
            throw new RuntimeException(format("Vehicle with id={%s} was not found", id));
        }

        // return response
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
